package ut.com.atlassian.stash.plugin.hook.impl;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.net.URI;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ThreadPoolExecutor;

import javax.ws.rs.core.HttpHeaders;

import com.atlassian.stash.user.StashUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.stash.content.Change;
import com.atlassian.stash.content.ChangeType;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.content.DetailedChangeset;
import com.atlassian.stash.content.DetailedChangesetsRequest;
import com.atlassian.stash.content.Path;
import com.atlassian.stash.content.SimplePath;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.plugin.hook.data.RestPostReceiveHook;
import com.atlassian.stash.plugin.hook.data.RestRefChange;
import com.atlassian.stash.plugin.hook.internal.RequestFactoryWebHookProcessor;
import com.atlassian.stash.plugin.hook.internal.WebHookRequest;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.data.RestChange;
import com.atlassian.stash.rest.data.RestChangeset;
import com.atlassian.stash.rest.data.RestDetailedChangeset;
import com.atlassian.stash.rest.data.RestPage;
import com.atlassian.stash.rest.data.RestPath;
import com.atlassian.stash.rest.data.RestRepository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.Person;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.util.Operation;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;

/**
 * Unit tests of {@link RequestFactoryWebHookProcessor}.
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class RequestFactoryWebHookProcessorTest {

	private RequestFactoryWebHookProcessor testedObject;

	@Mock
	private ApplicationPropertiesService applicationPropertiesService;
	@Mock
	private SecurityService securityService;
	@Mock
	private HistoryService historyService;
	@Mock
	private NavBuilder navBuilder;
	@Mock
	private RequestFactory<Request<?, Response>> requestFactory;
	@Mock
    private ApplicationProperties appProperties;
    @Mock
    private StashUser currentUser;

	/**
	 * Prepares test environment.
	 * 
	 * @throws Throwable
	 */
	@Before
	public void before() throws Throwable {
		// call all security callbacks
		doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				@SuppressWarnings("unchecked")
				Operation<Object, Throwable> operation = (Operation<Object, Throwable>) invocation.getArguments()[2];
				return operation.perform();
			}

		}).when(securityService)
				.doWithPermission(Mockito.anyString(), Mockito.<Permission> any(), Mockito.<Operation<Object, Throwable>> any());

		// returns default value for all plugin-properties
		doAnswer(new Answer<Integer>() {

			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				return (Integer) invocation.getArguments()[1];
			}

		}).when(applicationPropertiesService).getPluginProperty(Mockito.anyString(), Mockito.anyInt());

		// creates mock for immediate execution
		final ThreadPoolExecutor threadPoolExecutor = mock(ThreadPoolExecutor.class);
		doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				((Runnable) invocation.getArguments()[0]).run();
				return null;
			}

		}).when(threadPoolExecutor).execute(Mockito.<Runnable> any());
	
		testedObject = new RequestFactoryWebHookProcessor(applicationPropertiesService, requestFactory, appProperties, threadPoolExecutor) {
			// empty extension, which makes protected constructor accessible for call
		};
	}

	/**
	 * Test sending of single message.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSending() throws Exception {
		// data returned by mock and after that asserted as waited response
		class Data {
			
			int changesetsLimit = 100;
			int changesLimit = 500;

			URI hookUri = URI.create("http://mock.mo");

			// repository
			int repositoryId = 5;
			String repositoryScmId = "rep_smc_id";
			String repositorySlug = "rep_slug";
			String repositoryName = "rep_name";
			boolean repositoryForkable = true;
			boolean repositoryPublic = true;

			// author
			String authorName = "Janko Hrasko";
			String authorEmail = "janko.hrasko@slovakia.com";

			// changeset
			String changesetId = "0123321";
			String changesetMessage = "changeset message";
			Date changesetDate = new Date();

			// change
			Path changePath = new SimplePath("/change/path");
			ChangeType changeType = ChangeType.MODIFY;

			// ref.change
			String refChangeFrom = "0123234";
			RefChangeType refChangeType = RefChangeType.ADD;

		}
		final Data data = new Data();

		// repository behavior
		Repository repository = mock(Repository.class);
		doReturn(data.repositoryId).when(repository).getId();
		doReturn(data.repositoryScmId).when(repository).getScmId();
		doReturn(data.repositorySlug).when(repository).getSlug();
		doReturn(data.repositoryName).when(repository).getName();
		doReturn(data.repositoryPublic).when(repository).isPublic();
		doReturn(data.repositoryForkable).when(repository).isForkable();

		// changes behavior
		@SuppressWarnings("unchecked")
		Page<Change> changes = mock(Page.class);
		Change change = mock(Change.class);
		doReturn(true).when(changes).getIsLastPage();
		doReturn(singletonList(change)).when(changes).getValues();
		doReturn(data.changeType).when(change).getType();
		doReturn(data.changePath).when(change).getPath();

		// author behavior
		Person person = mock(Person.class);
		doReturn(data.authorName).when(person).getName();
		doReturn(data.authorEmail).when(person).getEmailAddress();

		// changesets behavior
		@SuppressWarnings("unchecked")
		Page<Changeset> changesets = mock(Page.class);
		Changeset changeset = mock(Changeset.class);
		doReturn(true).when(changesets).getIsLastPage();
		doReturn(singletonList(changeset)).when(changesets).getValues();
		doReturn(1).when(changesets).getSize();
		doReturn(repository).when(changeset).getRepository();
		doReturn(data.changesetId).when(changeset).getId();
		doReturn(data.changesetMessage).when(changeset).getMessage();
		doReturn(person).when(changeset).getAuthor();
		doReturn(data.changesetDate).when(changeset).getAuthorTimestamp();

		@SuppressWarnings("unchecked")
		Page<DetailedChangeset> detailedChangesets = mock(Page.class);
		DetailedChangeset detailedChangeset = mock(DetailedChangeset.class);
		doReturn(singletonList(detailedChangeset)).when(detailedChangesets).getValues();
		doReturn(changeset).when(detailedChangeset).getToCommit();
		doReturn(changes).when(detailedChangeset).getChanges();

		// ref. changes behavior
		RefChange refChange = mock(RefChange.class);
		doReturn(data.refChangeFrom).when(refChange).getFromHash();
		doReturn(data.changesetId).when(refChange).getToHash();
		doReturn(data.refChangeType).when(refChange).getType();

		// services' behavior
		ArgumentCaptor<DetailedChangesetsRequest> detailsRequest = ArgumentCaptor.forClass(DetailedChangesetsRequest.class);
		doReturn(changesets).when(historyService)
				.getChangesetsBetween(Mockito.<ChangesetsBetweenRequest> any(), Mockito.<PageRequest> any());
		doReturn(detailedChangesets).when(historyService)
				.getDetailedChangesets(detailsRequest.capture(), Mockito.<PageRequest> any());
		NavBuilder.Repo navRepo = mock(NavBuilder.Repo.class);
		doReturn(navRepo).when(navBuilder).repo(repository);
		NavBuilder.Changeset navChangeset = mock(NavBuilder.Changeset.class);
		doReturn(navChangeset).when(navRepo).changeset(data.changesetId);
		doReturn(navChangeset).when(navChangeset).change(data.changePath);
		
		// app properties mocking
		doReturn("Stash").when(appProperties).getDisplayName();
		doReturn("2.6.2").when(appProperties).getVersion();

		@SuppressWarnings("unchecked")
		final Request<?, Response> request = mock(Request.class);
		doReturn(request).when(requestFactory).createRequest(MethodType.POST, data.hookUri.toString());

		ArgumentCaptor<Object> requestEntity = ArgumentCaptor.forClass(Object.class);
		ArgumentCaptor<String> userAgentHeader = ArgumentCaptor.forClass(String.class);
		testedObject.processAsync(new WebHookRequest(securityService, historyService, navBuilder, data.changesetsLimit, data.changesLimit,
				repository, singletonList(refChange), data.hookUri, currentUser));
		Mockito.verify(request).setEntity(requestEntity.capture());
		Mockito.verify(request).execute(Mockito.<ResponseHandler<Response>> any());
		Mockito.verify(request).setHeader(Mockito.eq(HttpHeaders.USER_AGENT), userAgentHeader.capture());
		Mockito.verify(appProperties).getVersion();
		Mockito.verify(appProperties).getDisplayName();

		assertNotNull(requestEntity.getValue());
		assertEquals("Stash 2.6.2", userAgentHeader.getValue());
		assertEquals(500, detailsRequest.getValue().getMaxChangesPerCommit());
		RestPostReceiveHook restPostReceiveHook = (RestPostReceiveHook) requestEntity.getValue();

		// assert repo.
		RestRepository restRepository = (RestRepository) restPostReceiveHook.get("repository");
		assertEquals(data.repositoryScmId, restRepository.getScmId());
		assertEquals(data.repositorySlug, restRepository.getSlug());
		assertEquals(data.repositoryName, restRepository.getName());

		// assert ref change
		assertNotNull(restPostReceiveHook.get("refChanges"));
		@SuppressWarnings("unchecked")
		Iterable<RestRefChange> restRefChanges = (Iterable<RestRefChange>) restPostReceiveHook.get("refChanges");
		Iterator<RestRefChange> restRefChangesIterator = restRefChanges.iterator();
		assertTrue(restRefChangesIterator.hasNext());
		RestRefChange restRefChange = restRefChangesIterator.next();
		assertEquals(data.refChangeFrom, restRefChange.get("fromHash"));
		assertEquals(data.changesetId, restRefChange.get("toHash"));
		assertEquals(data.refChangeType.name(), restRefChange.get("type"));
		assertFalse(restRefChangesIterator.hasNext());

		// assert changesets
		assertNotNull(restPostReceiveHook.get("changesets"));
		@SuppressWarnings("unchecked")
		RestPage<DetailedChangeset> restChangesets = (RestPage<DetailedChangeset>) restPostReceiveHook.get("changesets");
		assertTrue((Boolean) restChangesets.get("isLastPage"));
		@SuppressWarnings("unchecked")
		Iterable<RestDetailedChangeset> restDetailedChangesets = (Iterable<RestDetailedChangeset>) restChangesets.get("values");
		Iterator<RestDetailedChangeset> restDetailedChangesetsIterator = restDetailedChangesets.iterator();
		assertTrue(restDetailedChangesetsIterator.hasNext());
		RestDetailedChangeset restDetailedChangeset = restDetailedChangesetsIterator.next();
		assertNotNull(restDetailedChangeset.get("toCommit"));
		RestChangeset toCommit = (RestChangeset) restDetailedChangeset.get("toCommit");
		assertEquals(data.changesetId, toCommit.get("id"));
		assertEquals(data.changesetMessage, toCommit.get("message"));
		assertEquals(data.changesetDate, toCommit.get("authorTimestamp"));
		assertFalse(restDetailedChangesetsIterator.hasNext());

		// assert changes
		assertNotNull(restDetailedChangeset.get("changes"));
		@SuppressWarnings("unchecked")
		RestPage<RestChange> restChanges = (RestPage<RestChange>) restDetailedChangeset.get("changes");
		@SuppressWarnings("unchecked")
		Iterator<RestChange> restChangesIterator = ((Iterable<RestChange>) restChanges.get("values")).iterator();
		assertTrue(restChangesIterator.hasNext());
		RestChange restChange = restChangesIterator.next();
		assertEquals(data.changePath.toString(), ((RestPath) restChange.get("path")).toString());
		assertEquals(data.changeType, restChange.get("type"));
		assertFalse(restChangesIterator.hasNext());

	}

}
