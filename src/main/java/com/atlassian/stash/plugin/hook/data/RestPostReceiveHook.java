package com.atlassian.stash.plugin.hook.data;

import com.atlassian.stash.rest.data.RestStashUser;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.atlassian.stash.rest.data.RestDetailedChangeset;
import com.atlassian.stash.rest.data.RestMapEntity;
import com.atlassian.stash.rest.data.RestPage;
import com.atlassian.stash.rest.data.RestRepository;

@JsonSerialize
public class RestPostReceiveHook extends RestMapEntity {

	private static final long serialVersionUID = 1L;

	public RestPostReceiveHook(RestRepository repository, Iterable<RestRefChange> refChanges, RestPage<RestDetailedChangeset> changesets,
                               RestStashUser currentUser) {
		put("repository", repository);
		put("refChanges", refChanges);
		put("changesets", changesets);
		putIfNotNull("user", currentUser);
	}

}
